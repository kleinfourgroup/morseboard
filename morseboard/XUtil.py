from Xlib import display
import Xlib
from Xlib import X
from Xlib import XK
import sys
import signal 
import time

dpy = None
root = None

grabbed = False
timer = None
t_reset = True

buff = ""

freq = "etaoinshrdlcumwfgypbvkjxqz0123456789"
alph = "abcdefghijklmnopqrstuvwxyz0123456789"

to_grab = ["m", "j", "k", "l", "i", "o", "p"]

buff_parse = {"mm": "a",
             "mk": "b",
             "ml": "c",
             "mi": "d",
             "mo": "e",
             "mp": "f"}

def get_code(key):
    keysym = XK.string_to_keysym(key)
    keycode = dpy.keysym_to_keycode(keysym)
    return keycode

def get_char(code):
    keysym = dpy.keycode_to_keysym(code, 0)
    for name in dir(XK):
        if name.startswith("XK_") and getattr(XK, name) == keysym:
            return name.lstrip("XK_")
    return "[%d]" % keysym

def get_type(etype):
    for name in dir(X):
        if name.startswith("Key") and getattr(X, name) == etype:
            return name
    return "[%d]" % etype
    
def grab(ch):
    code = get_code(ch)
    print "  Grabbing " + ch + " - " + str(code)
    root.grab_key(code, 0, True, X.GrabModeSync, X.GrabModeSync)

def ungrab(ch):
    code = get_code(ch)
    print "  Ungrabbing " + ch + " - " + str(code)
    root.ungrab_key(code, 0)

def grab_handler():
    global grabbed
    if not grabbed:
        for ch in to_grab:
            grab(ch)
    else:
        for ch in to_grab:
            ungrab(ch)
    grabbed = not grabbed

# ----------

def send_key(emulated_key, shift = False):
    shift_mask = 0 # or X.ShiftMask
    if shift:
        shift_mask = X.ShiftMask
    window = dpy.get_input_focus()._data["focus"]
    keysym = XK.string_to_keysym(emulated_key)
    keycode = dpy.keysym_to_keycode(keysym)
    event = Xlib.protocol.event.KeyPress(
        time = int(time.time()),
        root = root,
        window = window,
        same_screen = 0, child = X.NONE,
        root_x = 0, root_y = 0, event_x = 0, event_y = 0,
        state = shift_mask,
        detail = keycode
        )
    window.send_event(event, propagate = True)
    event = Xlib.protocol.event.KeyRelease(
        time = int(time.time()),
        root = dpy.screen().root,
        window = window,
        same_screen = 0, child = X.NONE,
        root_x = 0, root_y = 0, event_x = 0, event_y = 0,
        state = shift_mask,
        detail = keycode
        )
    window.send_event(event, propagate = True)
    print "Sent " + emulated_key + " " + str(shift)

from Xlib import display
import Xlib
from Xlib import X
from Xlib import XK
import sys
import signal 
import time


class board:
    def __init__(self):
        self.ctx = context.get_context()
        self.buff = buffer.get_buffer()
        self.grabbed = False
        self.timer = None
        self.t_reset = True
    def arm(self, sec = 10):
        signal.signal(signal.SIGALRM, lambda a,b:self.kill())
        signal.alarm(sec)

    def kill(self):
        print("killing")
        sys.exit(1)

    def grab_handler(self):
        if not self.grabbed:
            for ch in self.buff.to_grab:
                self.ctx.grab(ch)
            else:
                for ch in self.buff.to_grab:
                    self.ctx.ungrab(ch)
                    self.grabbed = not self.grabbed

    def handle_event(self, event):
        print("Handle: " + self.ctx.get_char(event.detail) + " " + self.ctx.get_type(event.type))
        if (self.ctx.get_type(event) == "KeyPress"):
            self.OnKeyPress(event)
        else:
            self.OnKeyUp(event)
        
    def OnKeyPress(self, event):
        if event.detail == self.ctx.get_code('grave'):
            if self.t_reset:
                self.timer = time.time()
                self.t_reset = False
                self.grab_handler()
        elif self.grabbed:
            ch = self.ctx.get_char(event.detail)
            self.buff.push(ch)
            stroke = self.buff.process()
            if not stroke == None:
                self.ctx.send_key(*stroke)

    def OnKeyUp(self, event):
        if event.detail == self.ctx.get_code('grave'):
            dif = time.time() - self.timer
            self.t_reset = True
            print("\t" + str(dif))
            if dif > 0.2:
                self.kill()

    def main(self):
        self.arm(45)
        print("Grabbing `")
        self.ctx.grab('grave')
        while True:
            event = self.ctx.next_event()
            self.handle_event(event)
            self.ctx.allow_events(X.AsyncKeyboard, X.CurrentTime)

if __name__ == '__main__':
    main()

# ----------

"""
a --> ml
b --> ik
c --> kp
d --> ki
e --> mm
f --> li

g --> lo
h --> kk
i --> mo
j --> io
k --> ii
l --> ko

m --> lk
n --> mp
o --> mi
p --> im
q --> om
r --> kl

s --> km
t --> mk
u --> lm
v --> il
w --> ll
x --> ip

y --> lp
z --> ok
0 --> ol
1 --> oi
2 --> oo
3 --> op

4 --> pm
5 --> pk
6 --> pl
7 --> pi
8 --> po
9 --> pp
"""

"""
a --> meli
b --> inka
c --> kapo
d --> kain
e --> meme
f --> liin

g --> liom
h --> kaka
i --> meom
j --> inom
k --> inin
l --> kaom

m --> lika
n --> mepo
o --> mein
p --> inme
q --> omme
r --> kali

s --> kame
t --> meka
u --> lime
v --> inli
w --> lili
x --> inpo

y --> lipo
z --> omka
0 --> omli
1 --> omin
2 --> omom
3 --> ompo

4 --> pome
5 --> poka
6 --> poli
7 --> poin
8 --> poom
9 --> popo
"""

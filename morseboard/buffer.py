import threading

def get_buffer():
    buff = Buffer()
    return buff

class Buffer:
    def __init__(self):
        self.freq = "etaoinshrdlcumwfgypbvkjxqz0123456789"
        self.alph = "abcdefghijklmnopqrstuvwxyz0123456789"
        self.to_grab = ["m", "j", "k", "l", "i", "o", "p"]
        self.buff_parse = {}
        let = [x for x in self.to_grab if not x == "j"]
        loc = 0
        for x in let:
            for y in let:
                # print "buff_parse[" + x + y + "] = freq[" + str(loc) + "] = " + freq[loc]
                self.buff_parse[x + y] = self.freq[loc]
                loc = loc + 1
        self.buff = ""
        self.lock = threading.Lock()
        self.seq = 0

    def push(self, ch):
        ret = None
        self.lock.acquire()
        try:
            ret = self.seq
            self.seq += 1
            self.buff = self.buff + ch
        finally:
            self.lock.release()
        return ret

    def process(self, seq = -1):
        ret = None
        self.lock.acquire()
        try:
            if seq == -1 or seq == self.seq:
                self.seq += 1
                if len(self.buff) < 2:
                    pass
                elif len(self.buff) == 2:
                    if self.buff[0] == 'j' and not self.buff[1] == 'j':
                        pass
                    elif self.buff in self.buff_parse:
                        ch = self.buff_parse[self.buff]
                        ret = (ch,)
                        self.buff = ""
                    else:
                        self.buff = self.buff[0]
                elif len(self.buff) == 3:
                    t = self.buff[1:]
                    if t in self.buff_parse:
                        ch = self.buff_parse[t]
                        ret = (ch, True)
                        self.buff = ""
                    else:
                        self.buff = self.buff[0]
                else:
                    self.buff = self.buff[0]
        finally:
            self.lock.release()
        return ret

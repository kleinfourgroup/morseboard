import Xlib
import time

def get_context():
    ctx = Xcontext()
    return ctx

class Xcontext:
    def __init__(self):
        self.display = Xlib.display.Display()
        self.root = self.display.screen().root
        self.root.change_attributes(event_mask = Xlib.X.KeyPressMask|Xlib.X.KeyReleaseMask)

    def next_event(self):
        event = self.display.next_event()
        return event
    
    def allow_event(self):
        self.display.allow_events(Xlib.X.AsyncKeyboard, Xlib.X.CurrentTime)
        
    def get_code(self, key):
        keysym = Xlib.XK.string_to_keysym(key)
        keycode = self.display.keysym_to_keycode(keysym)
        return keycode

    def get_char(self, code):
        keysym = self.display.keycode_to_keysym(code, 0)
        for name in dir(Xlib.XK):
            if name.startswith("XK_") and getattr(Xlib.XK, name) == keysym:
                return name.lstrip("XK_")
            return "[%d]" % keysym

    def get_type(self, event):
        for name in dir(Xlib.X):
            if name.startswith("Key") and getattr(Xlib.X, name) == event.type:
                return name
        return "[%d]" % event.type
    
    def grab(self, key):
        code = self.get_code(key)
        print("Grabbing " + key + " - " + str(code))
        self.root.grab_key(code, 0, True, Xlib.X.GrabModeSync, Xlib.X.GrabModeSync)

    def ungrab(self, key):
        code = self.get_code(key)
        print("Ungrabbing " + key + " - " + str(code))
        root.ungrab_key(code, 0)
        
    def send_key(self, emulated_key, shift = False):
        shift_mask = 0 # or Xlib.X.ShiftMask
        if shift:
            shift_mask = Xlib.X.ShiftMask
        window = self.display.get_input_focus()._data["focus"]
        keycode = self.get_code(emulated_key)
        event = Xlib.protocol.event.KeyPress(
            time = int(time.time()),
            root = root,
            window = window,
            same_screen = 0, child = Xlib.X.NONE,
            root_x = 0, root_y = 0, event_x = 0, event_y = 0,
            state = shift_mask,
            detail = keycode
        )
        window.send_event(event, propagate = True)
        event = Xlib.protocol.event.KeyRelease(
            time = int(time.time()),
            root = self.display.screen().root,
            window = window,
            same_screen = 0, child = Xlib.X.NONE,
            root_x = 0, root_y = 0, event_x = 0, event_y = 0,
            state = shift_mask,
            detail = keycode
        )
        window.send_event(event, propagate = True)
        print("Sent " + emulated_key + " " + str(shift))
